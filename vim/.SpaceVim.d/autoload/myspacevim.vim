function! myspacevim#before() abort
  set timeoutlen=600
  let g:neoformat_try_node_exe = 1

  let g:tidal_boot = "/home/rlafuente/nextcloud/tidal/config/BootTidal.hs"
endfunction

function! myspacevim#after() abort
  nmap <Leader>mdp :call PasteMDLink()<cr>
  nmap <Leader>mdf :call FormatMDLink()<cr>
endfunction

" Markdown links "

function GetURLTitle(url)
    " Bail early if the url obviously isn't a URL.
    if a:url !~ '^https\?://'
        return ""
    endif

    " Use Python/BeautifulSoup to get link's page title.
    let title = system("python3 -c \"import bs4, requests; print(bs4.BeautifulSoup(requests.get('" . a:url . "').content, 'lxml').title.text.strip())\"")

    " Echo the error if getting title failed.
    if v:shell_error != 0
        echom title
        return ""
    endif

    " Strip trailing newline
    return substitute(title, '\n', '', 'g')
endfunction

function PasteMDLink()
    let url = getreg("+")
    let title = GetURLTitle(url)
    let mdLink = printf("[%s](%s)", title, url)
    execute "normal! a" . mdLink . "\<Esc>"
endfunction
    
function FormatMDLink()
    let url = expand('<cWORD>')
    let title = GetURLTitle(url)
    let mdLink = printf("[%s](%s)", title, url)
    execute "normal! ?https\<cr>cW" . mdLink . "\<Esc>"
endfunction
