#!/bin/bash

git submodule add https://github.com/rlafuente/prezto.git zsh/.zprezto

stow */

# set up spacevim
curl -sLf https://spacevim.org/install.sh | bash
