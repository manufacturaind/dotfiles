local awful = require("awful")

function getHostname()
    local f = io.popen ("/bin/hostname")
    local hostname = f:read("*a") or ""
    f:close()
    hostname =string.gsub(hostname, "\n$", "")
    return hostname
end
local hostname = getHostname()

local function has_value (tab, val)
    for index, value in ipairs(tab) do
        if value == val then
            return true
        end
    end
    return false
end

-- {{{ Autostart windowless processes
local function run_once(cmd_arr)
    for _, cmd in ipairs(cmd_arr) do
        findme = cmd
        firstspace = cmd:find(" ")
        if firstspace then
            findme = cmd:sub(0, firstspace-1)
        end
        awful.spawn.with_shell(string.format("pgrep -u $USER -x -f %s > /dev/null || (%s)", findme, cmd))
    end
end

if hostname == "kinoko" then
  run_once({ "nm-tray", 
             "nextcloud", 
             "redshift -l 41.9:8.37", 
             "xbindkeys", 
  })
elseif has_value({"shikao", "marnie", "prince"}, hostname) then
  run_once({ 
             -- "xfdesktop --disable-wm-check",
             "xfsettingsd", 
             "nm-applet", 
             "blueman-applet", 
             "nextcloud", 
             "redshift -l 41.9:8.37", 
             "xbindkeys", 
             "xfce4-power-manager",
             "sudo powertop --auto-tune",
             "pasystray",
             "conky",
             "memorycheck",
             -- "unclutter -root", 
             -- "picom",
  })
elseif has_value({"pokkle"}, hostname) then
  run_once({ "unclutter -root", 
             "xfsettingsd", 
             "nm-applet", 
             -- "blueman-applet", 
             "nextcloud", 
             -- "redshift -l 41.9:8.37", 
             "xbindkeys", 
             "xfce4-power-manager",
             "sudo powertop --auto-tune",
             "pasystray",
             "conky",
             -- "picom",
  })
else
  run_once({ "unclutter -root", 
             "xfsettingsd", 
             "nm-applet", 
             "nextcloud", 
             "redshift -l 41.9:8.37", 
             "xbindkeys", 
             -- run steam
             -- run transmission
             -- "set-power-management"
  })
end

-- run_once("xrdb -merge .Xresources")
-- }}}

