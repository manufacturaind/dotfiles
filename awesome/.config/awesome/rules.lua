local awful = require("awful")
local beautiful = require("beautiful")

-- see also 
-- https://github.com/mphe/dotfiles/blob/master/configdir/awesome/rules.lua

-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = awful.client.focus.filter,
                     raise = true,
                     keys = clientkeys,
                     maximized_vertical = false,
                     maximized_horizontal = false,
                     buttons = clientbuttons,
                     screen = awful.screen.preferred,
                     placement = awful.placement.no_overlap+awful.placement.no_offscreen,
                     size_hints_honor = false
     }
    },

    -- Desktop {{{
    {
        rule_any = { class = { "xfdesktop" } },
        callback = function(c)
            c.screen = awful.screen.getbycoord(0, 0)
            end,
        properties = {
            tag = "1",
            sticky = true,
            border_width = 0,
            skip_taskbar = true,
            keys = {},
        }
    }, -- }}}

    -- Titlebars
    { rule_any = { type = { "dialog", "normal" } },
      properties = { titlebars_enabled = false } },

    -- Set Firefox to always map on tag 9 on screen 1.
    { rule = { class = "TelegramDesktop" },
      properties = { tag = awful.util.tagnames[2] } },
    { rule = { class = "Slack" },
      properties = { tag = awful.util.tagnames[3] } },
    { rule = { class = "Firefox" },
      properties = { screen = 1, tag = awful.util.tagnames[9] } },
    -- Maximized clients
    -- { rule_any = { class = { "Gimp", }, },
      -- properties = { maximized = true, } },
    -- Floating clients
    { rule_any = { class = { "Shoebot", "QjackCtl" }, },
      properties = { floating = true, } },
}
-- }}}
