These are our dotfiles for configuring our Debian system. Though they are 
tailored to our own specific needs, they're a handy shortcut to a prettier 
command-line experience.

## Installation

To clone this repository, be sure to use the `--recurse-submodules` flag when running `git clone`. Alternatively, after cloning just run `git submodule update --recursive --init`.

These dotfiles are set up to be installed via **GNU stow**. After installing the
`stow` package and cloning this repository, do

    cd dotfiles
    stow */

## Configure prezto

[Prezto](https://github.com/sorin-ionescu/prezto) (the zsh extras package) needs
some extra configuration:

    setopt EXTENDED_GLOB
    for rcfile in "${ZDOTDIR:-$HOME}"/.zprezto/runcoms/^README.md(.N); do
      ln -s "$rcfile" "${ZDOTDIR:-$HOME}/.${rcfile:t}"
      done
    # set zsh as default
    chsh -s /bin/zsh

## TODO

- use the `--dotfiles` option on stow so that hidden dirs can be named "dot-vim" etc (see the stow man page)
